class Platform {
  constructor(stripe) {
    this.stripe = stripe;
  }

  transfer(account, amount) {
    return this.stripe.transfers.create({
      amount: amount,
      currency: this.currency,
      destination: account.data.id,
    });
  }

  /**
   * Retrieves all platform's customers, 10 at a time.
   *
   * @returns Promise
   */
  getCustomers() {
    return new Promise((resolve, reject) => {
      this.stripe.customers.list(
        {
          limit: 10,
        },
        (err, customers) => {
          if (err) reject(err);
          resolve(customers.data);
        }
      );
    });
  }

  /**
   * Tries to retrieve a customer instance for the given Connect account
   *  and creates one if it doesn't exist.
   *
   * Mandatory data:
   *  - email
   *  - Name
   *  - Tax ID
   *
   * @param {*} email: The Customer's email address.
   * @returns Promise
   */
  getOrCreateCustomer(customer) {
    return new Promise((resolve, reject) => {
      this.stripe.customers.list(
        {
          email: customer.email,
          limit: 1,
        },
        (err, customers) => {
          if (err !== null) {
            reject(err);
            return;
          }

          if (customers.data.length === 0) {
            console.log(
              `Customer for ${customer.email} not found. Creating...`
            );
            
            this.stripe.customers.create(
              {
                email: customer.email,
                name: customer.name,
              },
              (err, newCustomer) => {
                if (err === null) reject(err);
                else resolve(newCustomer);
              }
            );
          } else {
            resolve(customers.data[0]);
          }
        }
      );
    });
  }

  /**
   * Finds Costumer instance by email.
   *
   * @param email The customer's email to find.
   * @returns Promise
   */
  getCustomerByEmail(email) {
    return new Promise((resolve, reject) => {
      this.stripe.customers.list(
        { email: email, limit: 1 },
        (err, customers) => {
          // An error was thrown by the Strip API so we interrupt the method's execution.
          if (err) return reject(err);

          // No customers were found by the fetch action, we also stop the method's execution.
          if (customers.data.length === 0)
            return reject(`Customer ${email} not found.`);

          // We only allow one Customer per email so return the first entry.
          resolve(customers.data[0]);
        }
      );
    });
  }

  /**
   * Remove a specific customer from the platform.
   *
   * @param string customerId The Customer's id.
   */
  removeCustomer(customerId) {
    this.stripe.customers.del(customerId, (err, confirmation) => {
      console.log(confirmation);
    });
  }
}

module.exports.Platform = Platform;
