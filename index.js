const stripe = require('stripe');
const { Platform } = require('./platform');
const { PaymentIntents } = require('./payments');
const { Invoice } = require('./invoices');

class StripePayments {
  constructor(stripeApiKey) {
    this.stripe = stripe(stripeApiKey);
    this.platform = new Platform(this.stripe);
    this.intents = new PaymentIntents(this.stripe);
    this.invoice = new Invoice(this.stripe);
  }
}

module.exports.StripePayments = StripePayments;
