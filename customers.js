const config = require('./config');
const stripe = require('stripe')(config.api_key);

ALLOWED_STATUSES = ['draft', 'open', 'paid', 'uncollectible', 'void'];

class Customer {

  constructor(customer){
    this.data = customer;
  }

  /**
   * Retrieves all invoices for the current customer.
   * 
   * If a status is passed and that status is a valid one,
   *  we filter the search by it.
   * 
   * @param {*} status 
   * @returns Promise with all the customer's invoices
   */
  getInvoices(status) {
    let data = { customer: this.data.id };
    if (typeof status !== 'undefined' && ALLOWED_STATUSES.indexOf(status) !== -1)
      data['status'] = status;

    return new Promise((resolve, reject) => {
      stripe.invoices.list(data, (err, invoices) => {
        if (err !== null) reject(err);
        else resolve(invoices.data);
      });
    });
  }
}

module.exports.Customer = Customer;