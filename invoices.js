class Invoice {
 
  constructor(stripe) {
    this.currency = 'eur';  // TODO: Check either items currency or customer's
    this.stripe = stripe;

    // Initialize instance properties
    this.items = [];
    this.taxRate = null;
    this.data = null;
  }

  /**
   * Create a new Invoice instance with Stripe API.
   * 
   * @returns Promise with the operation's status.
   */
  create(customer) {
    // This validation might not be adequate if invoice items are created prior to invoice.
    if (this.items.length === 0) {
      throw new Error('Create Invoice Items before creating an Invoice.');
    }

    let invoiceData = {
      customer: customer.id
    }

    if (this.taxRate) {
      invoiceData.default_tax_rates = [this.taxRate.id];
    }

    // Creates the Invoice instance with the registered LineItem instances and return the Promise.
    // We're overriding the Stripe promise to save the data in the class, for posterior usage.
    return new Promise((resolve, reject) => {
      this.stripe.invoices.create(
        invoiceData,
        (err, invoice) => {
          if (err !== null) {
            reject(err);
          } else {
            this.data = invoice;
            resolve(true);
          }
        }
      );
    });
  }

  /**
   * Finalize an invoice.
   * 
   * Stripe automatically finalizes drafts before sending and attempting payment on invoices. 
   * However, if you’d like to finalize a draft invoice manually, you can do so using this method.
   * 
   * @returns Promise with the operation's status.
   */
  finalize() {
    if (this.data === null)
      throw new Error('Invoice object not created. Call `create` first.');
      return new Promise((resolve, reject) => {
        this.stripe.invoices.finalizeInvoice(this.data.id, { auto_advance: false },
          (err, invoice) => {
          if (err !== null) {
            reject(err);
          } else {
            this.data = invoice;
            resolve(true);
          }
        });
      });
  }

  charge() {
    if (this.data === null) {
      throw new Error('Please create invoice before finalizing it.');
    }
    return this.stripe.invoices.pay(this.data.id);
  }

  /**
   * Creates and adds an LineItem instance to the the Invoice.
   * 
   * @param {String} description the item's description.
   * @param {Number} amount the item's price.
   */
  addItem(customer, description, amount) {
    return new Promise((resolve, reject) => {
      this.stripe.invoiceItems.create({
        customer: customer.id,
        amount: amount,
        currency: this.currency,
        description: description
      }, 
      (err, invoiceItem) => {
        if (err) {
          reject(err);
          return;
        }

        this.items.push(invoiceItem);
        resolve(invoiceItem);
      });
    });
  }

  /**
   * Remove the InvoiceItem from Stripe associated with the given id.
   * 
   * @param {string} itemId The invoice item id
   * @returns Promise
   */
  removeItem(itemId) {
    const item = this.items.find(el => el.id === itemId);
    this.items.splice(this.items.indexOf(item), 1);
    return this.stripe.invoiceItems.del(itemId);
  }

  /**
   * Retrieves all available InvoiceItem instances.
   * 
   * If there're no locally registered items try to fetch them from Stripe API.
   * 
   * @returns Promise
   */
  getItems() {
    return new Promise((resolve, reject) => {
      if (this.items.length !== 0) {
        resolve(this.items);
        return;
      }

      this.stripe.invoiceItems.list({}, (err, invoiceItems) => {
          if (err) {
            reject(err);
            return;
          }
          this.items = invoiceItems.data;
          resolve(invoiceItems.data);
        }
      );
    });
  }

  /**
   * Create a tax rate that can be associated with invoices.
   *
   * @param {*} tax
   */
  createTaxRate(tax) {
    return new Promise((resolve, reject) => {
      this.stripe.taxRates.create(tax, (err, taxRate) => {
        if (err !== null) {
          reject(err);
          return;
        } else {
          this.taxRate = taxRate;
          resolve(taxRate);
        }
      });
    });
  }

  async getTaxRate(taxRateId) {
    let taxRate = await this.stripe.taxRates.retrieve(taxRateId);
    this.taxRate = taxRate;
    return taxRate;
  }
}


module.exports.Invoice = Invoice;
