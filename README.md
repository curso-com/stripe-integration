# Stripe integration

## Run instructions

First create a file called `config.json` and add a property called `api_key` with your Stripe API key. 

Then, in your terminal of choice, run the following command: 

```
node test.js
```