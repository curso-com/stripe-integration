class PaymentIntents {
  constructor(stripe) {
    this.stripe = stripe;
    this.currency = 'eur';
  }

  /**
   * Create a new PaymentIntent instance associated with the
   * given Account and Customer.
   *
   * @param {*} paymentMethodId
   * @returns Promise
   */
  async create(customer, amount, paymentMethodId) {
    let paymentIntentData = {
      amount: amount,
      currency: this.currency,
      payment_method_types: ['card'],
      customer: customer.id
      // application_fee_amount: this.amount * APPLICATION_FEE, // Should the application fee be calculated with or without the Stripe fees?
    };

    // If a payment method id is provided use it a to create the PaymentIntent.
    if (typeof paymentMethodId !== 'undefined') {
      paymentIntentData['payment_method'] = paymentMethodId;
    }
    const paymentIntent = await this.stripe.paymentIntents.create(paymentIntentData);
    return paymentIntent;
  }

  /**
   * Lists all PaymentIntent instances associated with the current Account.
   * 
   * @returns Promise
   */
  list(customer) {
    return new Promise((resolve, reject) => {
      this.stripe.paymentIntents.list(
        {
          customer: customer.id,
          limit: 10,
        },
        (err, paymentIntents) => {
          if (err) reject(err);
          else resolve(paymentIntents.data);
        }
      );
    });
  }

  /**
   * Retrieves a PaymentIntent by id.
   * 
   * @param {*} paymentId 
   * @returns Promise
   */
  retrieve(paymentId) {
    return this.stripe.paymentIntents.retrieve(paymentId);
  }

  // Sources
  createSource(sourceData) {
    return this.stripe.sources.create(sourceData);
  }
}

module.exports.PaymentIntents = PaymentIntents;
