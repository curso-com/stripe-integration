const accounts = require('./accounts_data');
const { Account } = require('./accounts');
const { Platform } = require('./platform');
const { Payments } = require('./payments');
const { Invoice } = require('./invoices');
const { Customer } = require('./customers');

const TEST_CUSTOMER_EMAIL = 'gustavo.coelho.lima@gmail.com';
const TEST_CUSTOMER = {
  email: TEST_CUSTOMER_EMAIL,
  name: 'TEST_NAME',
  tax_id_data: [{ type: 'eu_vat', value: "000000000" }],
};
const TEST_ITEMS = [
  { description: 'Pricier second course', amount: 8000 }
];
const TEST_AMOUNT = 10000; // In cents.
const PT_TAX_RATE = 'txr_1ErNZrLdYEXwPsgDswxozHHv';
const PT_TAX_RATE_DATA = {
  display_name: 'VAT-pt',
  description: 'VAT Portugal',
  jurisdiction: 'PT',
  percentage: 23.0,
  inclusive: false,
};
const TEST_IMMEDIATE_PAYMENT_CARD = '4000 0000 0000 0077';


const platform = new Platform('eur');
// const account = new Account(accounts.PT_ACCOUNT_ID);
// account.create();
// account.remove();
// account.retrieve();

// _createBootCustomers();
// _reset();
_platformPayment();
// _getOpenInvoicesForCustomer();

async function _getOpenInvoicesForCustomer() {
  const platformCustomer = await platform.getOrCreateCustomer(TEST_CUSTOMER);
  const customer = new Customer(platformCustomer);
  const openInvoices = await customer.getInvoices('open');
  const payments = new Payments(platformCustomer);
  openInvoices.map(async invoice => {
    const pi = await payments.retrieve(invoice.payment_intent);
    console.log(`${pi.client_secret}: ${pi.amount}`);
  });
}

async function _createBootCustomers() {
  let platformCustomer;
  try {
    platformCustomer = await platform.getOrCreateCustomer(TEST_CUSTOMER);
  } catch(e) {
    console.error(e);
  }
}

async function _listPayments() {
  const platformCustomer = await platform.getOrCreateCustomer(TEST_CUSTOMER);
  const payments = new Payments(platformCustomer);
  const paymentsList = await payments.list();  
  paymentsList.map(item => console.log(item));
}

async function _platformPayment() {
  const platformCustomer = await platform.getOrCreateCustomer(TEST_CUSTOMER);
  const invoice = await _createInvoice(platformCustomer, TEST_ITEMS);
  await invoice.finalize();
  const payments = new Payments(platformCustomer);
  const paymentIntent = await payments.retrieve(invoice.data.payment_intent);
  console.log(paymentIntent);
}

/**
 * Creates an invoice for a given Customer instance.
 * 
 * @param {*} customer 
 */
function _createInvoice(customer, items) {
  if (items.length === 0)
    throw new Error('Please provide list with items for Invoice creation.');

  // Invoice line items
  let promisesList = [];
  const invoice = new Invoice(customer.id);

  // Create every line item to add to an invoice.
  for (const { description, amount } of items) {
    promisesList.push(invoice.addItem(description, amount));
  }

  return new Promise((resolve, reject) => {
    // We must wait until all InvoiceItem instances are 
    // created to request a new Invoice from Stripe.
    // Before creating the invoice we must check if the 
    // appropriate tax rate exists or if it needs to be created.
    Promise.all(promisesList).then(async () => {
      try {
        await invoice.getTaxRate(PT_TAX_RATE);
      } catch (err) {
        await invoice.createTaxRate(PT_TAX_RATE_DATA);  // User is PT so we must add the tax
      }
      await invoice.create();
      resolve(invoice);
    });
  });  
}

async function _removeInvoiceItems(account, customer) {
  const invoice = new Invoice(account, customer);
  const items = await invoice.getItems();
  for (const item of items) {
    invoice.removeItem(item.id);
  }
}

async function _reset() {
  const customers = await platform.getCustomers();
  for (const customer of customers) {
    try {
      account.removeCustomer(customer.id);
    } catch (e) {
      console.error(e);
    }

    const accountCustomer = await account.getCustomerByEmail(customer.email);
    
    try {
      if (accountCustomer) account.removeCustomer(accountCustomer.id);
    } catch (e) {
      console.error(e);
    }

    try {
      platform.removeCustomer(customer.id);
    } catch (e) {
      console.error(e);
    }
   }
}
