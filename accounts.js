const config = require('./config');
const stripe = require('stripe')(config.api_key);
const ip = require('ip');

class Account {
  constructor(data) {
    this.data = data;
    this.isInitialized = false;
  }

  retrieve() {
    return stripe.accounts.retrieve(this.data.id);
  }

  remove() {
    return stripe.accounts.del(this.data.id);
  }

  update(updateData) {
    return stripe.accounts.update(this.data.id, updateData);
  }

  /**
   * Create an user account.
   */
  create() {
    return new Promise((resolve, reject) => {
      stripe.accounts.create(
        {
          type: 'custom',
          country: this.data.country,
          business_type: 'individual',
          individual: this.data.individual,
          company: {
            tax_id: this.account.data.tax_id,
          },
        },
        (err, account) => {
          if (err) return reject(err);
          this.data.id = account.id;
          this.createBankAccount()
            .then(bankAccount => {
              console.log('Bank account Promise success', bankAccount);
              this.data.bank_account.id = bankAccount.id;
            })
            .catch(err => reject(err));
          this.setPerson();
          this.acceptTerms();
          resolve(true);
        }
      );
    });
  }

  /**
   * Accepts Stripe's Terms of Service for the connected account.
   */
  acceptTerms() {
    stripe.accounts.update(this.data.id, {
      tos_acceptance: {
        date: Math.floor(Date.now() / 1000),
        ip: ip.address(), // Assumes you're not using a proxy
      },
    });
  }

  /**
   * Creates a Person instance related to a given user account.
   */
  setPerson() {
    stripe.accounts.createPerson(
      this.data.id,
      {
        first_name: this.data.individual.first_name,
        last_name: this.data.individual.last_name,
        relationship: {
          account_opener: true,
          director: false,
          owner: true,
          percent_ownership: null,
          title: null,
        },
      },
      (err, person) => {
        console.log(err, person);
      }
    );
  }

  /**
   * Creates an external bank account for a specific user.
   *
   * @returns Promise
   */
  async createBankAccount() {
    return new Promise((resolve, reject) => {
      stripe.accounts.createExternalAccount(
        this.data.id,
        {
          external_account: {
            object: 'bank_account',
            country: this.data.country,
            currency: this.data.currency,
            account_number: this.data.bank_account.number,
          },
        },
        (err, bankAccount) => {
          if (err) reject(err);
          resolve(bankAccount);
        }
      );
    });
  }

  /**
   * Removes the current account's customer for the given id.
   *
   * @param {*} customerId
   * @returns Promise
   */
  removeCustomer(customerId) {
    return stripe.customers.del(customerId, { stripe_account: this.data.id });
  }

  /**
   * Adds a shared Customer instance to the Connected account.
   * 
   * First checks if the customer already exists on the Connected 
   * account and if not it creates it.
   *
   * @param customerId The Customer's instance id.
   * @returns The created Customer creation Promise, with the Customer information.
   */
  async getOrCreateCustomer(customer) {
    return new Promise(async (resolve, reject) => {
      try {
        let accountCustomer = await this.getCustomerByEmail(customer.email);
        return resolve(accountCustomer);        
      } catch (e) {
        console.warn(e.message);
      }

      // No Costumer was found for the given email, try to find via `id`.
      try {
        accountCustomer = await this.getCostumer(customer.id);
        return resolve(accountCustomer);
      } catch (e) {
        console.warn(e.message);
      }
      
      // No Costumer instance found, create one from given data.
      console.debug('Customer not found. Creating one...');
      resolve(this.createCustomer(customer));
    });
  }

  /**
   * Create a Customer instance on the Connected account from another Customer.
   * 
   * @param {*} customer 
   * @returns Promise
   */
  createCustomer(customer) {
    return new Promise((resolve, reject) => {
      stripe.tokens
        .create({ customer: customer.id }, { stripe_account: this.data.id })
        .then(token => {
          // Copy tax information from given customer.
          let taxInfo = [];
          for (const { type, value } of customer.tax_ids.data) {
            taxInfo.push({ type: type, value: value });
          }
          stripe.customers
            .create(
              {
                description: 'Shared customer',
                email: customer.email,
                name: customer.name,
                tax_id_data: taxInfo,
                source: token.id,
              },
              {
                stripe_account: this.data.id,
              }
            )
            .then(newCostumer => resolve(newCostumer));
        });
    });
  }

  /**
   * Find Costumer instance by `id`.
   *
   * @param costumerId A string defining the customer's id.
   * @returns A Promise with the customer's information.
   */
  getCostumer(costumerId) {
    return stripe.customers.retrieve(costumerId, {
      stripe_account: this.data.id,
    });
  }

  /**
   * Finds Costumer instance by email.
   *
   * @param email The customer's email to find.
   * @returns Promise
   */
  getCustomerByEmail(email) {
    return new Promise((resolve, reject) => {
      stripe.customers.list(
        { email: email, limit: 1 },
        { stripe_account: this.data.id },
        (err, customers) => {
          // An error was thrown by the Strip API so we interrupt the method's execution.
          if (err) return reject(err);

          // No customers were found by the fetch action, we also stop the method's execution.
          if (customers.data.length === 0)
            return reject(`Customer ${email} not found.`);
          resolve(customers.data[0]);
        }
      );
    });
  }

  /**
   * Retrieve all Customer  instances associated with current Connected account.
   *
   * @returns Promise
   */
  getCustomers() {
    return new Promise((resolve, reject) => {
      stripe.customers.list(
        { limit: 50 },
        { stripe_account: this.data.id },
        (err, customers) => {
          // An error was thrown by the Stripe API so we
          // interrupt the method's execution.
          if (err) reject(err);
          else resolve(customers.data);
        }
      );
    });
  }
}

module.exports.Account = Account;
